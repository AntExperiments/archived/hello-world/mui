import AppBar from '@mui/material/AppBar';
import { Card, CardMedia, CardContent, Button, Grid, IconButton, Typography, Toolbar, Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

export default () => {
  return (
    <div className="App">
      <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            News
          </Typography>
          <Button color="inherit" href="https://ant.lgbt" target="_blank">About</Button>
        </Toolbar>
      </AppBar>
    </Box>

      <Grid container spacing={4} p={3}>
        {[...Array(5).keys()].map(e => <Grid item xs={4}>
          <Card>
            <CardMedia
              component="img"
              height="200"
              image={`https://picsum.photos/800/400?r=${e}`}
              alt="random image"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Lizard
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Lizards are a widespread group of squamate reptiles, with over 6,000
                species, ranging across all continents except Antarctica
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        )}
      </Grid>
    </div>
  );
}